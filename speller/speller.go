//go:build !solution

package speller

import (
	"math"
	"strings"
)

var wordMap map[int64]string

func init() {
	wordMap = map[int64]string{
		0:          "zero",
		1:          "one",
		2:          "two",
		3:          "three",
		4:          "four",
		5:          "five",
		6:          "six",
		7:          "seven",
		8:          "eight",
		9:          "nine",
		10:         "ten",
		11:         "eleven",
		12:         "twelve",
		13:         "thirteen",
		14:         "fourteen",
		15:         "fifteen",
		16:         "sixteen",
		17:         "seventeen",
		18:         "eighteen",
		19:         "nineteen",
		20:         "twenty",
		30:         "thirty",
		40:         "forty",
		50:         "fifty",
		60:         "sixty",
		70:         "seventy",
		80:         "eighty",
		90:         "ninety",
		100:        "hundred",
		1000:       "thousand",
		1000000:    "million",
		1000000000: "billion",
	}
}

func Spell(n int64) string {
	if n == 0 {
		return wordMap[0]
	}
	separator := " "
	var spell strings.Builder
	var nOfPow10, nOf10 int64
	if n < 0 {
		spell.WriteString("minus")
		n *= -1
	}
	for _, pow := range []float64{9, 6, 3, 2} {
		pow10 := int64(math.Pow(10, pow))
		nOfPow10 = n / pow10
		if nOfPow10 > 0 {
			if spell.Len() > 0 {
				spell.WriteString(separator)
			}
			spell.WriteString(Spell(nOfPow10))
			spell.WriteString(separator)
			spell.WriteString(wordMap[pow10])
			n -= nOfPow10 * pow10
		}
	}
	nOf10 = n / 10
	if nOf10 > 1 {
		if spell.Len() > 0 {
			spell.WriteString(separator)
		}
		spell.WriteString(wordMap[nOf10*10])
		if n >= 21 {
			separator = "-"
		}
		n -= nOf10 * 10
	}
	if n > 0 {
		if spell.Len() > 0 {
			spell.WriteString(separator)
		}
		spell.WriteString(wordMap[n])
	}
	return spell.String()
}
