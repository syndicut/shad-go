//go:build !solution

package varfmt

import (
	"fmt"
	"strconv"
	"strings"
	"unicode"
)

type State int

const (
	initial = iota
	placeholderStarted
	seenInteger
)

const (
	placeholderStart = '{'
	placeholderEnd   = '}'
)

func Sprintf(format string, args ...interface{}) string {
	var lastIndex int
	var result strings.Builder
	result.Grow(len(format))
	stringArgs := make([]string, len(args))
	for i, arg := range args {
		stringArgs[i] = fmt.Sprint(arg)
	}
	var indexStart int
	var state State
	for i, r := range format {
		switch {
		case state == initial && r == placeholderStart:
			state = placeholderStarted
		case state == placeholderStarted && r == placeholderEnd:
			state = initial
			result.WriteString(fmt.Sprint(args[lastIndex]))
			lastIndex++
		case state == placeholderStarted && unicode.IsDigit(r):
			indexStart = i
			state = seenInteger
		case state == seenInteger && unicode.IsDigit(r):
			continue
		case state == seenInteger && r == placeholderEnd:
			index, _ := strconv.Atoi(format[indexStart:i])
			result.WriteString(stringArgs[index])
			lastIndex++
			state = initial
		default:
			result.WriteRune(r)
			state = initial
		}
	}
	return result.String()
}
