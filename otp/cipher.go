//go:build !solution

package otp

import (
	"io"
)

type Reader struct {
	reader io.Reader
	prng   io.Reader
}

func (r Reader) Read(p []byte) (n int, err error) {
	buf := make([]byte, len(p))
	n, err = r.reader.Read(buf)
	rnd := make([]byte, n)
	nprng, _ := r.prng.Read(rnd)
	if nprng < n {
		n = nprng
	}
	for i := 0; i < n; i++ {
		p[i] = buf[i] ^ rnd[i]
	}

	return
}

type Writer struct {
	writer io.Writer
	prng   io.Reader
}

func (w Writer) Write(p []byte) (n int, err error) {
	n = len(p)
	buf := make([]byte, n)
	rnd := make([]byte, n)
	nprng, _ := w.prng.Read(rnd)
	if nprng < n {
		n = nprng
	}
	for i := 0; i < len(p); i++ {
		buf[i] = p[i] ^ rnd[i]
	}

	return w.writer.Write(buf)
}

func NewReader(r io.Reader, prng io.Reader) io.Reader {
	return Reader{
		reader: r,
		prng:   prng,
	}
}

func NewWriter(w io.Writer, prng io.Reader) io.Writer {
	return Writer{
		writer: w,
		prng:   prng,
	}
}
