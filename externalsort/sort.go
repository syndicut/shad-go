//go:build !solution

package externalsort

import (
	"bufio"
	"bytes"
	"container/heap"
	"errors"
	"fmt"
	"io"
	"os"
	"slices"
)

type lineReader struct {
	scanner *bufio.Scanner
}

func (lr lineReader) ReadLine() (string, error) {
	ok := lr.scanner.Scan()
	if err := lr.scanner.Err(); err != nil {
		return "", fmt.Errorf("failed to scan next string: %w", err)
	}
	var err error
	if !ok {
		err = io.EOF
	}
	return lr.scanner.Text(), err
}

type lineWriter struct {
	writer io.Writer
}

func (lw lineWriter) Write(l string) error {
	_, err := fmt.Fprintln(lw.writer, l)
	if err != nil {
		return err
	}
	return nil
}

func scanLines(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	if i := bytes.IndexByte(data, '\n'); i >= 0 {
		// We have a full newline-terminated line.
		return i + 1, data[0:i], nil
	}
	// If we're at EOF, we have a final, non-terminated line. Return it.
	if atEOF {
		return len(data), data, nil
	}
	// Request more data.
	return 0, nil, nil
}

func NewReader(r io.Reader) LineReader {
	scanner := bufio.NewScanner(r)
	scanner.Split(scanLines)
	const maxCapacity int = 128 * 1024
	buf := make([]byte, maxCapacity)
	scanner.Buffer(buf, maxCapacity)

	return lineReader{
		scanner: scanner,
	}
}

func NewWriter(w io.Writer) LineWriter {
	return lineWriter{
		writer: w,
	}
}

type IndexString struct {
	Index  int
	String string
}

type IndexStringHeap []IndexString

func (h IndexStringHeap) Len() int           { return len(h) }
func (h IndexStringHeap) Less(i, j int) bool { return h[i].String < h[j].String }
func (h IndexStringHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *IndexStringHeap) Push(x any) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	*h = append(*h, x.(IndexString))
}

func (h *IndexStringHeap) Pop() any {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func Merge(w LineWriter, readers ...LineReader) error {
	h := &IndexStringHeap{}
	heap.Init(h)
	for i, reader := range readers {
		l, err := reader.ReadLine()
		if err != nil {
			if !errors.Is(err, io.EOF) {
				return fmt.Errorf("error reading line: %w", err)
			}
			if l != "" {
				heap.Push(h, IndexString{Index: i, String: l})
			}
			continue
		}
		heap.Push(h, IndexString{Index: i, String: l})
	}
	for h.Len() > 0 {
		is := heap.Pop(h).(IndexString)
		if err := w.Write(is.String); err != nil {
			return fmt.Errorf("failed to write string: %w", err)
		}
		l, err := readers[is.Index].ReadLine()
		if err != nil {
			if !errors.Is(err, io.EOF) {
				return fmt.Errorf("error reading line: %w", err)
			}
			if l != "" {
				heap.Push(h, IndexString{Index: is.Index, String: l})
			}
			continue
		}
		heap.Push(h, IndexString{Index: is.Index, String: l})
	}
	return nil
}

func Sort(w io.Writer, in ...string) error {
	files := make([]LineReader, 0, len(in))
	for _, filename := range in {
		err := sortFileInPlace(filename)
		if err != nil {
			return fmt.Errorf("failed to sort file in-place: %w", err)
		}
		file, err := os.Open(filename)
		if err != nil {
			return fmt.Errorf("failed to open file for reading %s: %w", filename, err)
		}
		files = append(files, NewReader(file))
	}
	return Merge(NewWriter(w), files...)
}

func sortFileInPlace(filename string) error {
	file, err := os.Open(filename)
	if err != nil {
		return fmt.Errorf("failed to open file for reading %s: %w", filename, err)
	}
	r := NewReader(file)
	lines := make([]string, 0)
	for {
		l, err := r.ReadLine()
		if err != nil {
			if !errors.Is(err, io.EOF) {
				return fmt.Errorf("failed to read line from file %s: %w", filename, err)
			}
			if l != "" {
				lines = append(lines, l)
			}
			break
		}
		lines = append(lines, l)
	}
	//for l, err := r.ReadLine(); !errors.Is(err, io.EOF); l, err = r.ReadLine() {
	//	lines = append(lines, l)
	//}
	if err != nil && !errors.Is(err, io.EOF) {
		return fmt.Errorf("failed to read line from file %s: %w", filename, err)
	}
	slices.Sort(lines)
	file, err = os.OpenFile(filename, os.O_WRONLY|os.O_TRUNC, 0666)
	if err != nil {
		return fmt.Errorf("failed to open file for writing %s: %w", filename, err)
	}
	w := NewWriter(file)
	for _, l := range lines {
		if err := w.Write(l); err != nil {
			return fmt.Errorf("failed to write string: %w", err)
		}
	}
	return nil
}
