//go:build !solution

package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	counter := make(map[string]int)
	for _, file := range os.Args[1:] {
		contents, err := os.ReadFile(file)
		if err != nil {
			fmt.Printf("error reading file: %s", err)
			os.Exit(1)
		}
		for _, line := range strings.Split(string(contents), "\n") {
			counter[line]++
		}
	}
	for line, count := range counter {
		if count > 1 {
			fmt.Printf("%d\t%s\n", count, line)
		}
	}
}
