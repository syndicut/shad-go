//go:build !solution

package lrucache

import (
	"container/list"
)

type cache struct {
	container *list.List
	cap       int
	index     map[int]*list.Element
}

type pair struct {
	key, value int
}

func (c *cache) Get(key int) (int, bool) {
	e, ok := c.index[key]
	if ok {
		c.container.MoveToFront(e)
		return e.Value.(pair).value, ok
	}
	return 0, false
}

func (c *cache) Set(key, value int) {
	// Special case for zero capacity case
	if c.cap == 0 {
		return
	}

	p := pair{key, value}
	e, ok := c.index[key]
	if ok {
		e.Value = p
		c.container.MoveToFront(e)
		return
	}
	if c.container.Len() == c.cap {
		last := c.container.Back()
		delete(c.index, last.Value.(pair).key)
		c.container.Remove(last)
	}
	e = c.container.PushFront(p)
	c.index[key] = e
}

func (c *cache) Range(f func(key int, value int) bool) {
	for e := c.container.Back(); e != nil; e = e.Prev() {
		p := e.Value.(pair)
		if ok := f(p.key, p.value); !ok {
			return
		}
	}
}

func (c *cache) Clear() {
	// TODO(syndicut): Or just use new? Not sure what is better for GC
	c.container.Init()
	c.index = make(map[int]*list.Element)
}

func New(cap int) Cache {
	return &cache{
		container: list.New(),
		cap:       cap,
		index:     make(map[int]*list.Element),
	}
}
