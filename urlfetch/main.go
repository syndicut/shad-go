//go:build !solution

package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

func urlfetch(url string) (err error) {
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("failed to get url: %w", err)
	}
	defer func() {
		err = resp.Body.Close()
	}()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("failed to read response body: %w", err)
	}
	fmt.Println(string(body))
	return nil
}

func main() {
	for _, url := range os.Args[1:] {
		if err := urlfetch(url); err != nil {
			fmt.Printf("failed to fetch url: %s", err)
			os.Exit(1)
		}
	}
}
