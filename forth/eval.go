//go:build !solution

package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

const sep = " "

var (
	ErrNotEnoughElements = errors.New("not enough elements on stack")
	ErrDivisionByZero    = errors.New("integer divide by zero")
)

type stackElements []int

type Evaluator struct {
	stack stackElements
	words map[string][]func() error
}

// NewEvaluator creates evaluator.
func NewEvaluator() *Evaluator {
	e := &Evaluator{}
	e.words = make(map[string][]func() error)
	return e
}

func add(i1 int, i2 int) (int, error) {
	return i1 + i2, nil
}

func sub(i1 int, i2 int) (int, error) {
	return i1 - i2, nil
}

func mul(i1 int, i2 int) (int, error) {
	return i1 * i2, nil
}

func div(i1 int, i2 int) (int, error) {
	if i2 == 0 {
		return 0, ErrDivisionByZero
	}
	return i1 / i2, nil
}

func (e *Evaluator) arithmeticOp(op func(int, int) (int, error)) error {
	if len(e.stack) < 2 {
		return ErrNotEnoughElements
	}
	res, err := op(e.stack[0], e.stack[1])
	if err != nil {
		return err
	}
	e.stack = append(e.stack[:len(e.stack)-2], res)
	return nil
}

func (e *Evaluator) dup() error {
	if len(e.stack) < 1 {
		return ErrNotEnoughElements
	}
	e.stack = append(e.stack, e.stack[len(e.stack)-1])
	return nil
}

func (e *Evaluator) over() error {
	if len(e.stack) < 2 {
		return ErrNotEnoughElements
	}
	e.stack = append(e.stack, e.stack[len(e.stack)-2])
	return nil
}

func (e *Evaluator) drop() error {
	if len(e.stack) < 1 {
		return ErrNotEnoughElements
	}
	e.stack = e.stack[:len(e.stack)-1]
	return nil
}

func (e *Evaluator) swap() error {
	if len(e.stack) < 2 {
		return ErrNotEnoughElements
	}
	e.stack = append(e.stack[:len(e.stack)-2], e.stack[len(e.stack)-1], e.stack[len(e.stack)-2])
	return nil
}

// Process evaluates sequence of words or definition.
//
// Returns resulting stack state and an error.
func (e *Evaluator) Process(row string) ([]int, error) {
	parts := strings.Split(row, sep)
	if parts[0] == ":" && parts[len(parts)-1] == ";" {
		if len(parts) < 4 {
			return nil, errors.New("cannot parse word definition with less then 4 parts")
		}
		wordName := strings.ToLower(parts[1])
		if _, err := strconv.Atoi(wordName); err == nil {
			return nil, errors.New("cannot redefine numbers")
		}
		var funcs []func() error
		for _, part := range parts[2 : len(parts)-1] {
			if word, ok := e.words[part]; ok {
				funcs = append(funcs, word...)
				continue
			}
			funcs = append(funcs, e.getFuncFor(part))
		}
		e.words[wordName] = funcs
		return e.stack, nil
	}
	for _, part := range parts {
		part = strings.ToLower(part)
		if word, ok := e.words[part]; ok {
			for _, f := range word {
				if err := f(); err != nil {
					return nil, err
				}
			}
			continue
		}
		f := e.getFuncFor(part)
		if err := f(); err != nil {
			return nil, err
		}
	}
	return e.stack, nil
}

func (e *Evaluator) getFuncFor(part string) func() error {
	part = strings.ToLower(part)
	switch {
	case part == "+":
		return func() error {
			return e.arithmeticOp(add)
		}
	case part == "-":
		return func() error {
			return e.arithmeticOp(sub)
		}
	case part == "*":
		return func() error {
			return e.arithmeticOp(mul)
		}
	case part == "/":
		return func() error {
			return e.arithmeticOp(div)
		}
	case part == "dup":
		return e.dup
	case part == "over":
		return e.over
	case part == "drop":
		return e.drop
	case part == "swap":
		return e.swap
	default:
		return func() error {
			number, err := strconv.Atoi(part)
			if err != nil {
				return fmt.Errorf("%s failed to convert to integer: %w", part, err)
			}
			e.stack = append(e.stack, number)
			return nil
		}
	}
}
