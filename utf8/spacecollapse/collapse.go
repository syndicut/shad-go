//go:build !solution

package spacecollapse

import (
	"strings"
	"unicode"
	"unicode/utf8"
)

func CollapseSpaces(input string) string {
	collapsed := strings.Builder{}
	collapsed.Grow(len(input))
	var lastRune rune
	for i := 0; i < len(input); {
		r, size := utf8.DecodeRuneInString(input[i:])
		i += size
		if unicode.IsSpace(r) {
			if lastRune == ' ' {
				continue
			}
			r = ' '
		}
		lastRune = r
		collapsed.WriteRune(r)
	}
	return collapsed.String()
}
