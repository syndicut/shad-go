//go:build !solution

package reverse

import (
	"strings"
	"unicode/utf8"
)

func Reverse(input string) string {
	reversed := strings.Builder{}
	reversed.Grow(len(input))
	for i := len(input); i > 0; {
		lastRune, size := utf8.DecodeLastRuneInString(input[:i])
		reversed.WriteRune(lastRune)
		i -= size
	}
	return reversed.String()
}
