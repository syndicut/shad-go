//go:build !solution

package hotelbusiness

import "sort"

type Guest struct {
	CheckInDate  int
	CheckOutDate int
}

type Load struct {
	StartDate  int
	GuestCount int
}

func ComputeLoad(guests []Guest) []Load {
	loads := []Load{}

	dateDiffIndex := map[int]int{}
	for _, guest := range guests {
		dateDiffIndex[guest.CheckInDate]++
		dateDiffIndex[guest.CheckOutDate]--
	}

	type dateDiff struct {
		Date int
		Diff int
	}
	sortedDateDiff := make([]dateDiff, 0, len(dateDiffIndex))
	for date, diff := range dateDiffIndex {
		if diff != 0 {
			sortedDateDiff = append(sortedDateDiff, dateDiff{date, diff})
		}
	}
	sort.Slice(sortedDateDiff, func(i, j int) bool {
		return sortedDateDiff[i].Date < sortedDateDiff[j].Date
	})

	var count int
	for _, diff := range sortedDateDiff {
		count += diff.Diff
		loads = append(loads, Load{diff.Date, count})
	}

	return loads
}
