//go:build !solution

package hogwarts

import "fmt"

func GetCourseList(prereqs map[string][]string) []string {
	courseList := []string{}
	courseListIdx := make(map[string]struct{})
	seen := make(map[string]struct{})
	var addReqs func([]string)
	addReqs = func(reqs []string) {
		for _, req := range reqs {
			if _, ok := seen[req]; ok {
				panic(fmt.Sprintf("found a loop, req: %s, seen: %s", req, seen))
			}
			seen[req] = struct{}{}
			if _, ok := prereqs[req]; ok {
				addReqs(prereqs[req])
			}
			if _, ok := courseListIdx[req]; !ok {
				courseList = append(courseList, req)
				courseListIdx[req] = struct{}{}
			}
			delete(seen, req)
		}
	}
	for course, reqs := range prereqs {
		addReqs(reqs)
		if _, ok := courseListIdx[course]; !ok {
			courseList = append(courseList, course)
			courseListIdx[course] = struct{}{}
		}
	}
	return courseList
}
